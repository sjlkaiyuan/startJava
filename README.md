# startJava

#### 介绍
为了方便部署springboot项目生成的jar文件


#### 使用说明

1.  ./start.sh 参数
2.  start 启动项目
3.  restart 重启项目
4.  stop 停止进程 使用kill -9 杀死进程（请自行修改）
5.  update  
    1）从git远程仓库 git pull 强制更新最新代码
    2）mvn 打包
    3) 运行restart
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

